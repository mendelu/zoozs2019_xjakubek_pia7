#include <iostream>

using namespace std;

class Package {
    string m_address;
    string m_email;
    string m_phone;
    float m_weight;

public:
    Package(string address) {
        m_address = address;
        m_email = "";
        m_phone = "";
        m_weight = 0.0;
    }

    Package(string address, float weight) {
        m_address = address;
        m_email = "";
        m_phone = "";
        m_weight = weight;
    }

    Package(string address, float weight, string email,
            string phone) {
        m_address = address;
        m_email = email;
        m_phone = phone;
        m_weight = weight;
    }

    Package(string address, float weight, string email) {
        m_address = address;
        m_email = email;
        m_phone = "";
        m_weight = weight;
    }

    void printInfo(){
        cout << "Address: " << m_address << endl;
        cout << "Email: " << m_email << endl;
        cout << "Phone: " << m_phone << endl;
        cout << "Weight: " << m_weight << endl;
    }
};


int main() {
    Package* myNewPhone;

    myNewPhone = new Package("Brno", 20);
    myNewPhone->printInfo();

    myNewPhone = new Package("Brno", 40,
            "xjakubek@mendelu.cz", "57700584");
    myNewPhone->printInfo();

    delete myNewPhone;
    return 0;
}