#include <iostream>

using namespace std;

class Developer {
private:
    string m_name;
    float m_baseSalary;
    float m_bonusRate;
    float m_workHours;
    float m_bugDeduction;
    int m_bugs;

public:
    Developer(string name, float salary) {
        m_name = name;
        setBaseSalary(salary);
        m_bonusRate = 500;
        m_bugDeduction = 1000;
        m_workHours = 0;
        m_bugs = 0;
    }

    Developer(string name, float salary, float bonus,
              float deduction) {
        m_name = name;
        setBaseSalary(salary);
        m_bonusRate = bonus;
        m_bugDeduction = deduction;
        m_workHours = 0;
        m_bugs = 0;
    }

    void setBaseSalary(float salary) {
        if (salary > 10000) {
            // je to ok
            m_baseSalary = salary;
        } else {
            // salary je 10000 alebo menej
            cout << "Minimal salary is 10001." << endl;
        }
    }

    void addBug() {
        m_bugs++;
        // nebo i:
        // m_bugs += 1;
        // m_bugs = m_bugs + 1;
    }

    float getSalary() {
        float result = 0;
        // pripocitame zakladny plat
        result += m_baseSalary;
        // ti co maju narok na bonus
        if (m_workHours > 40) {
            result += (m_workHours - 40) * m_bonusRate;
        }
        // bugy sa tykaju vsetkych
        result -= m_bugs * m_bugDeduction;
        return result;
    }

    void resetMonth() {
        m_bugs = 0;
        m_workHours = 0;
    }

    void setWorkHours(int hours) {
        m_workHours = hours;
    }

    void printInfo(){
        // TODO
    }
};

int main() {
    Developer *jano = new Developer("Jano", 15000);
    jano->addBug();
    jano->addBug();
    jano->addBug();
    jano->addBug();
    jano->addBug();

    float janoSalary = jano->getSalary();
    cout << "Jano zarobil v mesiaci 1: "<< janoSalary <<
    " KC." << endl;

    // dalsi mesiac
    jano->resetMonth();
    jano->addBug();
    jano->addBug();
    jano->setBaseSalary(22000);

    janoSalary = jano->getSalary();
    cout << "Jano zarobil v mesiaci 2: "<< janoSalary <<
         " KC." << endl;

    delete jano;
    return 0;
}