cmake_minimum_required(VERSION 3.14)
project(cv_03b)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv_03b main.cpp)