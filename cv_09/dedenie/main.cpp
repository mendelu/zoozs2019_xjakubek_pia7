#include <iostream>
#include <vector>
#include "Person.h"
#include "Student.h"
#include "Teacher.h"

int main() {
    std::vector<Person *> persons = {new Teacher("ucitel1", "1", "UI")};
    auto student1 = new Student("student1", "2", 1, 1);
    persons.push_back(student1);

    for (int i = 0; i < persons.size(); i++) {
        persons.at(i)->printInfo();
    }

    student1->printInfo();

    return 0;
}