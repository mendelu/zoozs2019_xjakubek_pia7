//
// Created by xjakubek on 22.11.2019.
//

#include "Teacher.h"

Teacher::Teacher(std::string name,
                 std::string id, std::string department) : Person(name, id) {
    setDepartment(department);
}

void Teacher::setDepartment(std::string department) {
    if (department == "") {
        std::cout << "Nespravny nazov ustavu!" << std::endl;
    } else {
        m_department = department;
    }
}

void Teacher::printInfo() {
    std::cout << "Som ucitel s menom " << std::endl;
}