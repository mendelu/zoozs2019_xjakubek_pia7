//
// Created by xjakubek on 22.11.2019.
//

#include "Person.h"

Person::Person(std::string name, std::string id) {
    m_name = name;
    m_id = id;
}

void Person::printInfo() {
    std::cout << "Som osoba s menom " << m_name << std::endl;
}

std::string Person::getId() {
    return m_id;
}

std::string Person::getName() {
    return m_name;
}
