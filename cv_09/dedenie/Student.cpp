//
// Created by xjakubek on 22.11.2019.
//

#include "Student.h"

Student::Student(std::string name, std::string id,
                 int semester, float average) : Person(name, id) {
    m_semester = semester;
    m_average = average;
}

void Student::addNextSemester() {
    m_semester += 1;
}

void Student::printInfo() {
    std::cout << "Som student s menom " << m_name << std::endl;
}