//
// Created by xjakubek on 22.11.2019.
//

#ifndef DEDENIE_TEACHER_H
#define DEDENIE_TEACHER_H

#include "Person.h"

class Teacher : public Person {
    std::string m_department;
public:
    Teacher(std::string name, std::string id, std::string department);

    void setDepartment(std::string department);

    void printInfo();
};


#endif //DEDENIE_TEACHER_H
