//
// Created by xjakubek on 22.11.2019.
//

#ifndef DEDENIE_PERSON_H
#define DEDENIE_PERSON_H

#include <iostream>
// using namespace std;

class Person {
protected:
    std::string m_name;
    std::string m_id;
public:
    Person(std::string name, std::string id);

    std::string getName();

    std::string getId();

    void printInfo();
};


#endif //DEDENIE_PERSON_H
