//
// Created by xjakubek on 22.11.2019.
//

#ifndef DEDENIE_STUDENT_H
#define DEDENIE_STUDENT_H

#include "Person.h"

class Student : public Person {
    int m_semester;
    float m_average;
public:
    Student(std::string name, std::string id, int semester, float average);

    void addNextSemester();

    void printInfo();
};


#endif //DEDENIE_STUDENT_H
