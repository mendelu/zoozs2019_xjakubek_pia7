#include <iostream>

using namespace std;

class ErrorLog {
    static ErrorLog *s_log;
    string m_errors;

    ErrorLog() {
        m_errors = "Error log: \n";
    }

public:
    static ErrorLog *getErrorLog() {
        if (s_log == nullptr) {
            s_log = new ErrorLog();
        }
        return s_log;
    }

    void logError(string where, string what) {
        m_errors += "- " + where + ": " + what + "\n";
    }

    string getErrors() {
        return m_errors;
    }
};

ErrorLog *ErrorLog::s_log = nullptr;

class Dragon {
    int m_lifes;
    int m_power;
    int m_defense;
public:
    Dragon(int power, int defense) {
        setPower(power);
        setDefense(defense);
        m_lifes = 100;
    }

    int getPower() {
        return m_power;
    }

    int getLifes() {
        return m_lifes;
    }

    int getDefense() {
        return m_defense;
    }

    void reduceLifes(int count) {
        m_lifes -= count;
        // m_lifes = m_lifes - count;
    }

private:
    void setPower(int power) {
        if ((power >= 0) and (power <= 100)) {
            // <0,100>
            m_power = power;
        } else {
            // mene nez 0, nebo vice jak 100
            // zalogujem chybu
            ErrorLog *logger = ErrorLog::getErrorLog();
            logger->logError("Dragon::setPower", "Power is"
                                                 "out of range"
                                                 "<0,100>. Set to"
                                                 "0.");
            m_power = 0;
        }
    }

    void setDefense(int defense) {
        // TODO to iste co power, len je iny interval
        // a ina log sprava
    }
};

class Knight {
    int m_lifes;
    int m_power;
    int m_defense;
    string m_name;

public:
    Knight(string name, int power, int defense) {
        m_name = name;
        setPower(power);
        m_defense = defense;
        m_lifes = 100;
    }

    int getPower() {
        return m_power;
    }

    int getLifes() {
        return m_lifes;
    }

    int getDefense() {
        return m_defense;
    }

    string getName() {
        return m_name;
    }

    void fight(Dragon *drak) {
        int dragonPower = drak->getPower();
        if (dragonPower > m_defense) {
            // drak je silnejsi
            // znizim pocet zivotov rytiera
            m_lifes -= dragonPower - m_defense;
        }
        int dragonDefense = drak->getDefense();
        if (dragonDefense < m_power) {
            // rytier je silnejsi
            // znizim pocet zivotov drakovi
            drak->reduceLifes(m_power - dragonDefense);
        }
    }

private:
    void setPower(int power) {
        if ((power >= 0) and (power <= 100)) {
            // <0,100>
            m_power = power;
        } else {
            // mene nez 0, nebo vice jak 100
            // zalogujem chybu
            ErrorLog *logger = ErrorLog::getErrorLog();
            logger->logError("Knight::setPower", "Power is"
                                                 "out of range"
                                                 "<0,100>. Set to"
                                                 "0.");
            m_power = 0;
        }
    }
};

int main() {
    Dragon *smak = new Dragon(80, 60);
    Knight *artus = new Knight("Artus", 70, 50);

    artus->fight(smak);
    cout << artus->getName() << ": " << artus->getLifes()
         << endl;
    cout << "Smak: " << smak->getLifes() << endl;

    ErrorLog*log = ErrorLog::getErrorLog();
    cout << log->getErrors();

    delete smak;
    delete artus;
    return 0;
}