//
// Created by xjakubek on 13.12.2019.
//

#ifndef CV_11_RYTIERBUILDER_H
#define CV_11_RYTIERBUILDER_H

#include "Rytier.h"

class RytierBuilder {
protected:
    Rytier *m_rytier;
public:
    Rytier *createRytier(std::string meno, int sila);

    Rytier *getRytier();

    virtual void buildBrnenie(int vaha, int odolnost) = 0;

    virtual void buildHelma(std::string velkost) = 0;
};


#endif //CV_11_RYTIERBUILDER_H
