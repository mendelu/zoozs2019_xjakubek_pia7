//
// Created by xjakubek on 13.12.2019.
//

#ifndef CV_11_RYTIER_H
#define CV_11_RYTIER_H

#include "Helma.h"
#include "Brnenie.h"

class Rytier {
private:
    Brnenie *m_brnenie;
    Helma *m_helma;
    std::string m_meno;
    int m_sila;

public:
    Rytier(std::string meno, int sila);

    void printInfo();

    void setBrnenie(Brnenie *brnenie);

    void setHelma(Helma *helma);

    int getObrana();

    int getUtok();
};


#endif //CV_11_RYTIER_H
