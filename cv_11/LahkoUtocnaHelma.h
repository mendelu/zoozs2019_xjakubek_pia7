//
// Created by xjakubek on 13.12.2019.
//

#ifndef CV_11_LAHKOUTOCNAHELMA_H
#define CV_11_LAHKOUTOCNAHELMA_H

#include "Helma.h"

class LahkoUtocnaHelma : public Helma {
public:
    LahkoUtocnaHelma(std::string velkost);

    int getBonusObrany() override;

    void printInfo() override;
};


#endif //CV_11_LAHKOUTOCNAHELMA_H
