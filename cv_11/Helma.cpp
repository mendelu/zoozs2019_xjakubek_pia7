//
// Created by xjakubek on 13.12.2019.
//

#include "Helma.h"

Helma::Helma(std::string velkost) {
    // kontrola vstupu
    m_velkost = velkost;
}

void Helma::printInfo() {
    std::cout << "helma: " << std::endl;
    std::cout << " - velkost helmy: " << m_velkost << std::endl;
    std::cout << " - bonus obrany: " << getBonusObrany() << std::endl;
}
