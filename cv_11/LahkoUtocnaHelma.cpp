//
// Created by xjakubek on 13.12.2019.
//

#include "LahkoUtocnaHelma.h"

LahkoUtocnaHelma::LahkoUtocnaHelma(std::string velkost): Helma(velkost) {
    // nemam ziadne dalsie parametre ktore je potrebne nastavit
}

int LahkoUtocnaHelma::getBonusObrany() {
    return 0;
}

void LahkoUtocnaHelma::printInfo() {
    Helma::printInfo();
    std::cout << " - typ: lahko utocna helma" << std::endl;
}
