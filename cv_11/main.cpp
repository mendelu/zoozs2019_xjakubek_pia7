#include <iostream>

#include "RytierDirectory.h"
#include "RytierBuilder.h"
#include "LahkoOddenecBuilder.h"
#include "TazkoOddenecBuilder.h"

int main() {
    RytierBuilder* stavitelLehkych = new LahkoOddenecBuilder();
    RytierDirectory* director = new RytierDirectory(stavitelLehkych);

    Rytier* artus = director->createRytier("ARTUS",50, 5, 10, "L");
    artus->printInfo();

    director->setRytierBuilder( new TazkoOddenecBuilder() );
    Rytier* jason = director->createRytier("JASON", 70, 25, 20, "XL");
    jason->printInfo();


    delete artus;
    delete stavitelLehkych;
    delete director;
    return 0;
}