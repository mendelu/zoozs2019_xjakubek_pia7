//
// Created by Tomas on 13. 12. 2019.
//

#include "LahkoOddenecBuilder.h"

void LahkoOddenecBuilder::buildBrnenie(int vaha, int odolnost) {
    m_rytier->setBrnenie(new KruzkoveBrnenie(vaha, odolnost, 0));
}

void LahkoOddenecBuilder::buildHelma(std::string velkost) {
    m_rytier->setHelma(new LahkoUtocnaHelma(velkost));
}