//
// Created by xjakubek on 13.12.2019.
//

#ifndef CV_11_BRNENIE_H
#define CV_11_BRNENIE_H

#include <iostream>

class Brnenie {
private:
    int m_vaha;
protected:
    int m_odolnost;
public:
    Brnenie(int vaha, int odolnost);

    virtual int getBonusObrany() = 0;

    virtual int getBonusUtoku() = 0;

    virtual void printInfo();
};


#endif //CV_11_BRNENIE_H
