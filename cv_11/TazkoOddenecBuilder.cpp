//
// Created by Tomas on 13. 12. 2019.
//

#include "TazkoOddenecBuilder.h"

void TazkoOddenecBuilder::buildBrnenie(int vaha, int odolnost) {
    m_rytier->setBrnenie(new PlatoveBrnenie(vaha, odolnost));
}

void TazkoOddenecBuilder::buildHelma(std::string velkost) {
    m_rytier->setHelma(new TazkoObrannaHelma(velkost, 0));
}