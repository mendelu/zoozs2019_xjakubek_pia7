//
// Created by xjakubek on 13.12.2019.
//

#ifndef CV_11_PLATOVEBRNENIE_H
#define CV_11_PLATOVEBRNENIE_H

#include "Brnenie.h"

class PlatoveBrnenie: public Brnenie {
public:
    PlatoveBrnenie(int vaha, int odolnost);

    int getBonusUtoku() override;

    int getBonusObrany() override;

    void printInfo() override;
};


#endif //CV_11_PLATOVEBRNENIE_H
