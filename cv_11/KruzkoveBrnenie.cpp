//
// Created by xjakubek on 13.12.2019.
//

#include "KruzkoveBrnenie.h"

KruzkoveBrnenie::KruzkoveBrnenie(int vaha,
                                 int odolnost,
                                 int ohybnost) : Brnenie(vaha,
                                                         odolnost) {
    m_ohybnost = ohybnost;
}

int KruzkoveBrnenie::getBonusUtoku() {
    return m_odolnost / 2 + m_ohybnost;
}

int KruzkoveBrnenie::getBonusObrany() {
    return m_odolnost + m_ohybnost / 2;
}

void KruzkoveBrnenie::printInfo() {
    Brnenie::printInfo();
    std::cout << " - typ: kruzkove brnenie" << std::endl;
}
