//
// Created by Tomas on 13. 12. 2019.
//

#include "RytierDirectory.h"

RytierDirectory::RytierDirectory(RytierBuilder *builder) {
    m_rytirBuilder = builder;
}

void RytierDirectory::setRytierBuilder(RytierBuilder *newBuilder) {
    m_rytirBuilder = newBuilder;
}

Rytier *RytierDirectory::createRytier(std::string meno, int sila, int vahaBrnenia, int odolnostBrnenia,
                                      std::string velkostHelmy) {
    m_rytirBuilder->createRytier(meno, sila);
    m_rytirBuilder->buildBrnenie(vahaBrnenia, odolnostBrnenia);
    m_rytirBuilder->buildHelma(velkostHelmy);

    return m_rytirBuilder->getRytier();
}