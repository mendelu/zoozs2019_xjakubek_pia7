//
// Created by Tomas on 13. 12. 2019.
//

#ifndef CV_11_RYTIERDIRECTORY_H
#define CV_11_RYTIERDIRECTORY_H

#include "RytierBuilder.h"

class RytierDirectory {

private:
    RytierBuilder *m_rytirBuilder;

public:
    RytierDirectory(RytierBuilder *builder);

    void setRytierBuilder(RytierBuilder *newBuilder);

    Rytier *createRytier(std::string meno, int sila, int vahaBrnenia, int odolnostBrnenia, std::string velkostHelmy);
};


#endif //CV_11_RYTIERDIRECTORY_H
