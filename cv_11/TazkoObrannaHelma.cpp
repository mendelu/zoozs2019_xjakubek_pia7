//
// Created by xjakubek on 13.12.2019.
//

#include "TazkoObrannaHelma.h"

TazkoObrannaHelma::TazkoObrannaHelma(std::string velkost,
        int bonusObrany): Helma(velkost) {
    m_bonusObrany = bonusObrany;
}

int TazkoObrannaHelma::getBonusObrany() {
    return m_bonusObrany;
}

void TazkoObrannaHelma::printInfo() {
    Helma::printInfo();
    std::cout << " - typ: tazko obranna helma" << std::endl;
}