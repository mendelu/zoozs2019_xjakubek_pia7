//
// Created by Tomas on 13. 12. 2019.
//

#ifndef CV_11_TAZKOODDENECBUILDER_H
#define CV_11_TAZKOODDENECBUILDER_H

#include "RytierBuilder.h"
#include "PlatoveBrnenie.h"
#include "TazkoObrannaHelma.h"

class TazkoOddenecBuilder : public RytierBuilder {

public:
    void buildBrnenie(int vaha, int odolnost) override;

    void buildHelma(std::string velkost) override;
};


#endif //CV_11_TAZKOODDENECBUILDER_H
