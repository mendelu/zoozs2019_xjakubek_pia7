#include <iostream>
#include <array>
#include <vector>

using namespace std;

class Box {
    float m_weight;
    string m_content;
    string m_owner;

public:
    Box(float w, string c, string o) {
        m_weight = w;
        m_content = c;
        m_owner = o;
    }

    string getContent() {
        return m_content;
    }

    string getOwner() {
        return m_owner;
    }
};

class Floor {
    string m_label;
    array<Box *, 10> m_position;
public:
    Floor(string label) {
        m_label = label;
        // POZOR! & pouzivame lebo chceme menit pole!!!
        for (Box *&position:m_position) {
            position = nullptr;
        };
    }

    void saveBox(int index, Box *newBox) {
        if ((index >= 0) and (index < 10)) {
            if (m_position.at(index) == nullptr) {
                // pridam
                m_position.at(index) = newBox;
            } else {
                cout << "There is box at position " << index << "! \n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void removeBox(int index) {
        if ((index >= 0) and (index < 10)) {
            if (m_position.at(index) != nullptr) {
                // odoberem
                m_position.at(index) = nullptr;
            } else {
                cout << "There is no box at position " << index << "! \n";
            }
        } else {
            cout << "You are removing out of range!" << endl;
        }
    }

    void printInfo() {
        cout << endl << "Floor status: " << endl;
        for (Box *position:m_position) {
            if (position != nullptr) {
                cout << position->getOwner() << ": " <<
                     position->getContent() << endl;
            } else {
                cout << "Position is empty!" << endl;
            }
        }
    }

    ~Floor() {
        for (auto *box:m_position) {
            delete (box);
        }
    }
};

class Store {
    vector<Floor *> m_floors;
public:
    Store() {
        m_floors.push_back(new Floor("Floor n.0"));
    }

    void buildNewFloor() {
        m_floors.push_back(new Floor("Floor n." + to_string(m_floors.size())));
    }

    void destroyLastFloor() {
        delete (m_floors.at(m_floors.size() - 1));
        m_floors.pop_back();
    }

    void storeBox(int floor, int position, Box *box) {

        // TODO kontrola, ci je floor v rozpeti
        m_floors.at(floor)->saveBox(position, box);
    }

    void removeBox(int floor, int position) {
        m_floors.at(floor)->removeBox(position);
    }

    void printInfo() {
        cout << "Store info: " << endl;
        for (int i = 0; i < m_floors.size(); i++) {
            m_floors[i]->printInfo();
        }
    }

    ~Store() {
        for (Floor *floor:m_floors) {
            delete (floor);
        }
    }

};

int main() {
    Box *boxik = new Box(100, "TVs", "Jozef Siroky");
//    Floor *f1 = new Floor("floor A");
//    f1->saveBox(0, boxik);
//    f1->printInfo();
//
//    // presun z pozicie 0 na poziciu 1
//    f1->removeBox(0);
//    f1->saveBox(1, boxik);
//
//    f1->printInfo();

    Store *store = new Store();
    store->buildNewFloor();
    store->buildNewFloor();
    store->buildNewFloor();
    store->destroyLastFloor();
    store->storeBox(0, 5, boxik);
    store->storeBox(1, 0, boxik);

    store->printInfo();
    delete boxik;
    delete store;
//    delete f1;
    return 0;
}