/*
 * Priklad 1:
 * Ukazka prace s terminalom (vystup, vstup)
 */

// Najdi knihovnu iostream (ktora spristupnuje vstupne a vystupne operacie)
// a vloz jej obsah priamo na toto miesto.
#include <iostream>

// Urcenie menneho priestoru. `Std` - standardna knihovna, ktora je sucastou
// kazdeho kompilatoru. Odtial su objekty ako `cout`, `cin`, `endl`, `string`.
// Inak by sme museli vzdy pri pouziti objektu z kniznice `std` to explicitne
// urcit: `std::cout`, `std::string`, `std::endl`, ...
using namespace std;

// Hlavna funkcia programu, ktora sa zavola automaticky ako prva po spusteni
// programu - kazdy program v C++ ma zaciatok programu - funkciu `main()`.
int main() {

    // deklarovanie premmenej `meno`, ktora je typu `string`
    string meno;
    // `cout` vypise na terminal retazec, ktory nasleduje za operatorom presmerovania
    // vystupu `<<`. `\t` - prida tabulator do vypisu
    cout << "Zadaj svoje meno: \t";
    // `cin` precita vstup z terminalu od uzivatela a ulozi ho do premennej `meno`,
    // ktoru sme deklarovali vyssie
    cin >> meno;

    int vek;
    cout << "Zadaj svoj vek: \t";
    cin >> vek;

    int hranica = 18;
    cout << "Aka ja hranica pre alhokol: \t";
    cin >> hranica;

    // `endl` je manipulacny objekt, ktory predstavuje prechod na novy riadok.
    // Rovnaky efekt je mozne dosiahnut pouzitim `\n` v retazci:
    // `cout << "prvy riadok \n druhy riadok";`
    cout << endl;

    // hodnota premmennej `meno`, ktoru sme vyssie nacitali od uzivatela sa vlozi
    // do vystupu. Za `meno` sa dalej vlozi textovy retazec. Uvedenym sposobom
    // je mozne retazit lubovolny pocet textov a hodnot.
    cout << meno << ", vitaj v nasom bare! Dnes ";

    // podmienka, ktora porovnava zadany `vek` a hodnotu v premennej `hranica`.
    // Vysledkom podmienky je rozhodnutie, v ktorom z nasledujich blokov (vetiev)
    // ma program pokracovat - ci ma ist do vetvy `if` alebo do vetvy `else`.
    if (vek < hranica) {
        // `vek` je mensi ako `hranica`, program bude pokracovat v tomto bloku

        // do premennej `rozdiel` sa vlozi vysledok aritmetickeho vyrazu `hranica - vek`
        int rozdiel = hranica - vek;
        cout << "mozes pit len kofolu. Pivo mozes az za " << rozdiel << " rok/y/ov.";
    } else {
        // `vek` je rovny alebo vacsi ako `hranica`, program pokracuje vo vetve `else`

        cout << "si mozes dat aj male pivo :).";
    }

    // funkcia `main()` vracia celociselnu hodnotu, konkretne hodnotu 0, ktora je informaciou
    // pre OS a znamena koniec programu.
    return 0;

    // prikazy, ktore su sa klucovym slovom `return` sa uz neprevolaju.
    cout << "Toto sa uz nevypise!!";
} // zatvorka, ktora predstavuje koniec funkcii `main()`