/**
 * Priklad 2:
 * Objekt triedy Auto bude mat atributy spz, rychlost a priznak, ze ci je alebo nie je auto v pohybe.
 * Obsahuje taktiez metody, pre nastavenie SPZ a nastavenie pohybu. Metoda `zmenRychlost()` bude
 * menit aktualnu rychlost auta po vstupe uzivatela a bude kontrolovat, ci nebola porusena povolena rychlost.
 * Ak bola, tak auto zastavi. Trieda bude obsahovat metodu pre vypis informacii o aute s nazvom `printInfo`.
 */
#include <iostream>

using namespace std;

// Deklaraciu triedy zaciname klucovym slovom `class`, za ktorym nasleduje jej nazov
class Auto {
// do casti public pride vsetko, co ma byt vidiet vo vnutri triedy
public:
    // deklaracia atributov m_spz, m_rychlost a m_vPohybe
    string m_spz;
    int m_rychlost;
    // datovy typ `bool`, ktory moze nadobudnut len 2 hodnoty:
    // 1. `true` = auto je v pohybe
    // 2. `false` = auto nie je v pohybe (stoji)
    bool m_vPohybe;

    /**
     * SET = nastav
     * Klicove slovo `void` povie prekladacu, ze tato metoda nic nevracia. A preto tu
     * mozme robit co chceme (napriklad nastavovat atribut/y). Typicky metoda prijima
     * aspon 1 parameter, ktory je uvedeny v zatvorke.
     */
    void setSpz(string novaSpz) {
        this->m_spz = novaSpz;
    }

    /**
     * GET = vrat
     * `string` hovori, ze metoda po zavolani vrati retazec.
     */
    string getSpz() {
        return m_spz;
    }

    // SET = nastav
    void setVPohybe(bool jeVPohybe) {
        m_vPohybe = jeVPohybe;
    }

    /**
     * print = tiskni/vypis
     * Metoda, ktora nic nevracia (navratovy typ je `void`) a nic nenastavuje (nie su
     * ziadne parametre v zatvorke).
     */
    void printInfo() {
        cout << "INFO: ";
        if (m_vPohybe == true) {
            // vyraz v podmienke je pravdivy (auto je v pohybe), program pokracuje sem

            cout << "Auto s SPZ " << getSpz() << " ide aktualnou rychlostou " << m_rychlost << endl;
        } else {
            // vyraz v podmienke nie je pravdivy (auto nie je v pohybe), program pokracuje sem

            cout << "Auto s SPZ " << m_spz << " stoji na mieste.\n";
        }
    }

    void zmenRychlost() {
        cout << "Nova rychlost auta: ";
        int rychlost;
        cin >> rychlost;

        if (rychlost > 50) {
            // auto ide 51, 52, ...
            cout << "Auto prekrocilo rychlost. Zastavujem!" << endl;
            // Auto prekrocilo rychlost a chceme ho zastavit. Preto zavolame metodu `setVPohybe()`,
            // ktorej predame hodnotu `false`, cim auto zastavime.
            setVPohybe(false);
        } else {
            // auto ide rychlostou 1,2 az 50 (vratane 50)
            cout << "Auto ide rychlostou " << rychlost << endl;
            m_rychlost = rychlost;
        }
    }
};

// hlavna funkcia programu `main()`, ktora je mimo triedy `Auto`
int main() {
    // deklaracia premmenej `ferrari`, typom je trieda objektu
    Auto *ferrari;

    // vytvorenie noveho objektu pomocou klucoveho slova `new`
    ferrari = new Auto();

    // nastavenie atributov objektu
    ferrari->setSpz("Brno123");
    ferrari->setVPohybe(true);

    // vypis informacii o objekte
    ferrari->printInfo();

    // zmena rychlosti auta
    ferrari->zmenRychlost();

    // vypis informacii o objekte
    ferrari->printInfo();

    // ukoncenie aplikacie
    return 0;
}