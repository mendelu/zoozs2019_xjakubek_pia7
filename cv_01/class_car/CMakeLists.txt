cmake_minimum_required(VERSION 3.14)
project(class_car)

set(CMAKE_CXX_STANDARD 14)

add_executable(class_car main.cpp)