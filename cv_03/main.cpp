/**
 * Budeme dnes evidovat pracu vyvojarov. Preto si vytvorime triedu Developer. Pri navrhu budeme mysliet
 * na zapuzdrenie.
 * Tato trieda ma nasledujuce vlastnosti:
 * - zakladny plat
 * - bonus za hodinu k zakladnemu platu podla vykonu. Tento bonus dostava ked odpracuje aspon 40 hodin
 * - pocet odpracovanych hodin
 * - ciastka, ktora sa strhne z platu za kazdu najdenu jeho chybu (bug)
 * - pocet chyb
 * - meno vyvojara
 *
 * Pri vytvarani noveho vyvojara si mozme vynutit:
 * - meno a zakladny plat vyvojara
 * - meno, zakladny plat, bonus a ciastku za bugy
 *
 * Vytvorte metodu, ktora umozni nastavit vysku zakladneho platu aspon 10 000. Pre
 * mensie platy napise chybou hlasku. Tuto metodu pouzite.
 *
 * Vytvorte metodu, ktora po zavolani zvysi pocet bugov. Tato metoda sa bude volat, ked
 * niekto najde v kode vyvojara bug.
 *
 * Dalej vytvorte metodu, ktora spocita a vrati presny plat vyvojara. Plat musi zahrnat vsetky
 * platove polozky, a to vratane hodinoveho bonusu k zakladnemu platu, ktory vyvojar ziska, ked
 * odpracuje aspon 40 hodin a takisto vratene znizenej ciastky v zavislosti na pocte bugov.
 *
 * Dalej vytvorte metodu, ktora sa zavola vzdy na konci mesiaca a zaruci, ze sa potrebne pocitadla
 * vynuluju a praca vyvojara sa bude zaznamenavat od zaciatku.
 *
 * Vytvorte metodu ktora bude umoznovat nastavovat a menit pocet odpracovanych hodin.
 *
 * Vytvorte metodu, ktora umozni vypsat na obrazovku veskere informacie o vyvojarovi.
 */

#include <iostream>

using namespace std;

class Developer {
private:
    float m_baseSalary;
    float m_bonusRate;
    float m_workHours;
    float m_bugDeduction;
    int m_bugs;
    string m_name;

public:
    Developer(string name, float salary) {
        // inicializujeme vsechno!!!
        m_name = name;
        setBaseSalary(salary);
        m_bonusRate = 500;
        m_bugDeduction = 1000;
        m_workHours = 0;
        m_bugs = 0;
    }

    Developer(string name, float salary, float bonus,
              float deduction) {
        // inicializujeme vsechno!!!
        m_name = name;
        setBaseSalary(salary);
        m_bonusRate = bonus;
        m_bugDeduction = deduction;
        m_workHours = 0;
        m_bugs = 0;
    }

    void setBaseSalary(float salary) {
        if (salary < 10000) {
            // salary je menej ako 10 000
            cout << "Prilis nizky plat!" << endl;
        } else {
            // salary je 10 000 alebo viac
            m_baseSalary = salary;
        }
    }

    void addBug() {
        m_bugs++;
        // m_bugs += 1;
        // m_bugs = m_bugs + 1;
    }

    float getSalary() {
        // toto bude nas vysledok
        float salary;
        // pridame base salary
        salary = m_baseSalary;
        // vyriesime bonusy
        if (m_workHours > 40) {
            salary += (m_workHours - 40) * m_bonusRate;
        }
        // vyriesime bugy
        salary -= m_bugs * m_bugDeduction;

        return salary;
    }

    void resetMonth() {
        m_workHours = 0;
        m_bugs = 0;
    }

    void setWorkHours(float hours) {
        m_workHours = hours;
    }

    void printInfo() {
        cout << "Name: " << m_name << endl;
        // vsetky ostatne atributy
        // TODO
    }
};


int main() {
    Developer *tomas = new Developer("Tomas", 20000, 500, 300);
    tomas->setWorkHours(50);
    tomas->addBug();
    tomas->addBug();
    tomas->addBug();
    tomas->addBug();
    tomas->addBug();
    float salary = tomas->getSalary();
    cout << "Month 1: " << salary << endl;

    // druhy mesiac
    tomas->resetMonth();
    tomas->setBaseSalary(25000);
    tomas->setWorkHours(30);
    tomas->addBug();
    tomas->addBug();
    salary = tomas->getSalary();
    cout << "Month 2: " << salary << endl;

    delete tomas;
    return 0;
}