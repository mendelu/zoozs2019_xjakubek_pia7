//
// Created by xjakubek on 15.11.2019.
//

#include "Book.h"

int Book::s_booksCount = 0;

Book::Book(string author, string title) {
    m_author = author;
    m_title = title;
    m_id = s_booksCount;
    s_booksCount += 1;
}

void Book::printInfo() {
    cout << "Book info: " << endl;
    cout << " - author: " << m_author << endl;
    cout << " - title: " << m_title << endl;
    cout << " - id: " << m_id << endl;
}

string Book::getAuthor() {
    return m_author;
}

int Book::getId() {
    return m_id;
}