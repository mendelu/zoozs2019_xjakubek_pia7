#include <iostream>

#include "Inventory.h"

int main() {
    Inventory::createBook("Jozo", "Neviem ZOO");
    Inventory::createBook("Fero", "Ja ho viem");
    Inventory::createBook("Jozo", "Tak sa to naucim");

    Inventory::removeBook(1);
    Inventory::printInfo();

    return 0;
}