//
// Created by xjakubek on 15.11.2019.
//

#ifndef CV_07_INVENTORY_H
#define CV_07_INVENTORY_H

#include <iostream>
#include <vector>
#include "Book.h"

using namespace std;

class Inventory {
    static vector<Book *> s_books;
private:
    Inventory() {};
//    Inventory() = default;
public:
    static void addBook(Book *newBook);

    static void removeBook(int id);

    static vector<Book *> searchByAuthor(string author);

    static void createBook(string author, string title);

    static void printInfo();
};


#endif //CV_07_INVENTORY_H
