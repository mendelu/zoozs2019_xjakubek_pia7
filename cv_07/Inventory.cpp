//
// Created by xjakubek on 15.11.2019.
//

#include "Inventory.h"

vector<Book *> Inventory::s_books = {};

void Inventory::createBook(string author, string title) {
    s_books.push_back(new Book(author, title));
}

void Inventory::addBook(Book *newBook) {
    s_books.push_back(newBook);
}

void Inventory::removeBook(int id) {
    for (int i = 0; i < s_books.size(); i++) {
        if (s_books.at(i)->getId() == id) {
            delete (s_books.at(i));
            s_books.erase(s_books.begin() + i);
            return;
        }
    }
}

vector<Book *> Inventory::searchByAuthor(string author) {
    vector<Book *> results = {};
    for (auto book:s_books) {
        if (book->getAuthor() == author) {
            results.push_back(book);
        }
    }

    return results;
}

void Inventory::printInfo() {
    for (int i = 0; i < s_books.size(); i++) {
        s_books.at(i)->printInfo();
    }
}
