//
// Created by xjakubek on 15.11.2019.
//

#ifndef CV_07_BOOK_H
#define CV_07_BOOK_H

#include <iostream>

using namespace std;

class Book {
    string m_title;
    string m_author;
    int m_id;
    static int s_booksCount;
public:
    Book(string author, string title);

    void printInfo();

    string getAuthor();

    int getId();
};


#endif //CV_07_BOOK_H
