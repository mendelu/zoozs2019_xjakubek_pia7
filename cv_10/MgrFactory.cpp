//
// Created by xjakubek on 03.12.2019.
//

#include "MgrFactory.h"

Student* MgrFactory::createStudent(string name) {
    Student* newStudent = Student::createStudent(name,
            StudyType::Mgr);
    return newStudent;
}

Course* MgrFactory::createCourse(string name, int credits) {
    return new Course(name, credits, StudyType::Mgr);
}