//
// Created by xjakubek on 03.12.2019.
//

#ifndef CV_10_STUDYFACTORY_H
#define CV_10_STUDYFACTORY_H

#include "Student.h"
#include "Course.h"

class StudyFactory {
public:
    virtual Student* createStudent(string name) = 0;
    virtual Course* createCourse(string name, int credits) = 0;
    // nezabudnut na virt. destruktor, inak sa nebudu
    // prevolat destruktory potomkov
    virtual ~StudyFactory(){}
};


#endif //CV_10_STUDYFACTORY_H
