//
// Created by xjakubek on 03.12.2019.
//

#include "Course.h"

Course::Course(string name, int credits, StudyType type) {
    m_name = name;
    m_credits = credits;
    m_type = type;
}

string Course::getName() {
    return m_name;
}

int Course::getCredits() {
    return m_credits;
}

StudyType Course::getStudyType() {
    return m_type;
}