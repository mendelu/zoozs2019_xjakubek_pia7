//
// Created by xjakubek on 03.12.2019.
//

#include "Student.h"

Student::Student(string name, float scholarship,
                 int studyLength, bool discount) {
    m_name = name;
    m_scholarshipPerYear = scholarship;
    m_standardStudyLength = studyLength;
    m_mealDiscount = discount;
}

Student* Student::createStudent(string name, StudyType type) {
    Student* newStudent = nullptr;

    switch(type){
        case StudyType::Bc:
            newStudent = new Student(name, 8000, 3, true);
            break;
        case StudyType::Mgr:
            newStudent = new Student(name, 2000, 2, true);
            break;
        case StudyType::Phd:
            newStudent = new Student(name, 0, 3, false);
            break;
        default:
            cout << "Unknown study type!!!" << endl;
            break;
    }

    return newStudent;
}

string Student::getName() {
    return m_name;
}

bool Student::getMealDiscount() {
    return m_mealDiscount;
}

float Student::getScholarshipPerYear() {
    return m_scholarshipPerYear;
}

int Student::getStandardStudyLength() {
    return m_standardStudyLength;
}
